## vNEXT

* When copying or slicing container values, metadata is now copied over
  instead of cleared. This makes it easier to propagate metadata.
  This also means one should make sure to update the metadata in the
  new container value to reflect changes to the value itself.
* `DataMetadata` now has `set_for_value` method to make a copy of
  metadata and set new `for_value` value. You can use this when you
  made a new value and you want to copy over metadata, but you also
  want this value to be associated with metadata. This is done by
  default for container values.
* Metadata now includes SHA256 digest for primitives and datasets.
  It is computed automatically during loading. This should allow one to
  track exact version of primitive and datasets used.
  `d3m_metadata.container.dataset.get_d3m_dataset_digest` is a reference
  implementation of computing digest for D3M datasets.
* Datasets can be now loaded in "lazy" mode: only metadata is loaded
  when creating a `Dataset` object. You can use `is_lazy` method to
  check if dataset iz lazy and data has not yet been loaded. You can use
  `load_lazy` to load data for a lazy object, making it non-lazy.
* There is now an utility metaclass `d3m_metadata.utils.AbstractMetaclass`
  which makes classes which use it automatically inherit docstrings
  for methods from the parent. Primitive base class and some other D3M
  classes are now using it.
* `d3m_metadata.metadata.CONTAINER_SCHEMA_VERSION` and
  `d3m_metadata.metadata.DATA_SCHEMA_VERSION` were fixed to point to the
  correct URI.
* Many `data_metafeatures` properties in metadata schema had type
  `numeric` which does not exist in JSON schema. They were fixed to
  `number`.
* Added `https://metadata.datadrivendiscovery.org/types/Target` and
  `https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint` to
  a list of known semantic types.
* Added to `algorithm_types`: `ARRAY_SLICING`,
  `ROBUST_PRINCIPAL_COMPONENT_ANALYSIS`, `SUBSPACE_CLUSTERING`,
  `SPECTRAL_CLUSTERING`.

## v2018.1.26

* Bumped `numpy` dependency to `1.14` and `pandas` to `0.22`.
* Added `https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter` as a known URI
  for `semantic_types` to help convey which hyper-parameters control the use of resources by the
  primitive.
  [#41](https://gitlab.com/datadrivendiscovery/metadata/issues/41)
* Fixed use of `numpy` values in `Params` and `Hyperparams`.
  [#39](https://gitlab.com/datadrivendiscovery/metadata/issues/39)
* Added `upper_inclusive` argument to `UniformInt`, `Uniform`, and `LogUniform` classes
  to signal that the upper bound is inclusive (default is exclusive).
  [#38](https://gitlab.com/datadrivendiscovery/metadata/issues/38)
* Made `semantic_types` and `description` keyword-only arguments in hyper-parameter description classes.
* Made all enumeration metadata classes have their instances be equal to their string names.
* Made sure `Hyperparams` subclasses can be pickled and unpickled.
* Improved error messages during metadata validation.
* Documented common metadata for primitives and data in the README.
* Added standard deviation to aggregate metadata values possible.
* Added `NO_JAGGED_VALUES` to `preconditions` and `effects`.
* Added to `algorithm_types`: `AGGREGATE_FUNCTION`, `AUDIO_STREAM_MANIPULATION`, `BACKWARD_DIFFERENCE_CODING`,
  `BAYESIAN_LINEAR_REGRESSION`, `CATEGORY_ENCODER`, `CROSS_VALIDATION`, `DISCRETIZATION`, `ENCODE_BINARY`,
  `ENCODE_ORDINAL`, `FEATURE_SCALING`, `FORWARD_DIFFERENCE_CODING`, `FREQUENCY_TRANSFORM`, `GAUSSIAN_PROCESS`,
  `HASHING`, `HELMERT_CODING`, `HOLDOUT`, `K_FOLD`, `LEAVE_ONE_OUT`, `MERSENNE_TWISTER`, `ORTHOGONAL_POLYNOMIAL_CODING`,
  `PASSIVE_AGGRESSIVE`, `PROBABILISTIC_DATA_CLEANING`, `QUADRATIC_DISCRIMINANT_ANALYSIS`, `RECEIVER_OPERATING_CHARACTERISTIC`,
  `RELATIONAL_DATA_MINING`, `REVERSE_HELMERT_CODING`, `SEMIDEFINITE_EMBEDDING`, `SIGNAL_ENERGY`, `SOFTMAX_FUNCTION`,
  `SPRUCE`, `STOCHASTIC_GRADIENT_DESCENT`, `SUM_CODING`, `TRUNCATED_NORMAL_DISTRIBUTION`, `UNIFORM_DISTRIBUTION`.
* Added to `primitive_family`: `DATA_GENERATION`, `DATA_VALIDATION`, `DATA_WRANGLING`, `VIDEO_PROCESSING`.
* Added `NoneType` to the list of data types allowed inside container types.
* For `PIP` dependencies specified by a `package_uri` git URI, an `#egg=package_name` URI suffix is
  now required.

## v2018.1.5

* Made use of the PyPI package official. Documented a requirement for
  `--process-dependency-links` argument during installation.
  [#27](https://gitlab.com/datadrivendiscovery/metadata/issues/27)
* Added `https://metadata.datadrivendiscovery.org/types/TuningParameter` and
  `https://metadata.datadrivendiscovery.org/types/ControlParameter` as two known URIs for
  `semantic_types` to help convey which hyper-parameters are true tuning parameters (should be
  tuned during hyper-parameter optimization phase) and which are control parameters (should be
  determined during pipeline construction phase and are part of the logic of the pipeline).
* Made `installation` metadata optional. This allows local-only primitives.
  You can still register them into D3M namespace using `d3m.index.register_primitive`.
* Fixed serialization to JSON of hyper-parameters with `q` argument.
* Clarified that primitive's `PIP` dependency `package` has to be installed with `--process-dependency-link` argument
  enabled, and `package_uri` with both `--process-dependency-link` and `--editable`, so that primitives can have access
  to their git history to generate metadata.
* Only `git+http` and `git+https` URI schemes are allowed for git repository URIs for `package_uri`.
* Added to `algorithm_types`: `AUDIO_MIXING`, `CANONICAL_CORRELATION_ANALYSIS`, `DATA_PROFILING`, `DEEP_FEATURE_SYNTHESIS`,
  `INFORMATION_ENTROPY`, `MFCC_FEATURE_EXTRACTION`, `MULTINOMIAL_NAIVE_BAYES`, `MUTUAL_INFORMATION`, `PARAMETRIC_TRAJECTORY_MODELING`,
  `SIGNAL_DITHERING`, `SIGNAL_TO_NOISE_RATIO`, `STATISTICAL_MOMENT_ANALYSIS`, `UNIFORM_TIMESERIES_SEGMENTATION`.
* Added to `primitive_family`: `SIMILARITY_MODELING`, `TIMESERIES_CLASSIFICATION`, `TIMESERIES_SEGMENTATION`.

## v2017.12.27

* Added `Params` class.
* Removed `Graph` class in favor of NetworkX `Graph` class.
* Added `Metadata` class with subclasses and documented the use of selectors.
* Added `Hyperparams` class.
* Added `Dataset` class.
* "Sequences" have generally been renamed to "containers". Related code is also now under
  `d3m_metadata.container` and not under `d3m_metadata.sequence` anymore.
* Package now requires Python 3.6.
* `__metadata__` attribute was renamed to `metadata`.
* Package renamed from `d3m_types` to `d3m_metadata`.
* Repository migrated to gitlab.com and made public.
* Added schemas for metadata contexts.
* A problem schema parsing and Python enumerations added in
  `d3m_metadata.problem` module.
* A standard set of container and base types have been defined.
